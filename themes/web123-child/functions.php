<?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );



function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');


add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
   if ( get_post_type() == 'advices' ) {
       if ( is_single() ) {
           // checks if the file exists in the theme first,
           // otherwise serve the file from the plugin
           if ( $theme_file = locate_template( array ( 'custom_advice.php' ) ) ) {
               $template_path = $theme_file;
           } else {
               $template_path = plugin_dir_path( __FILE__ ) . 'templates/custom_advice.php';
           }
       }
   }
   return $template_path;
}

add_filter( 'template_include', 'include_template_function_second', 1 );
function include_template_function_second( $template_path ) {
   if ( get_post_type() == 'post' ) {
       if ( is_single() ) {
           // checks if the file exists in the theme first,
           // otherwise serve the file from the plugin
           if ( $theme_file = locate_template( array ( 'single_blog.php' ) ) ) {
               $template_path = $theme_file;
           } else {
               $template_path = plugin_dir_path( __FILE__ ) . 'templates/single_blog.php';
           }
       }
   }
   return $template_path;
}


add_filter( 'template_include', 'include_template_function_third', 1 );
function include_template_function_third( $template_path ) {
   if ( get_post_type() == 'eventone' ) {
       if ( is_single() ) {
           // checks if the file exists in the theme first,
           // otherwise serve the file from the plugin
           if ( $theme_file = locate_template( array ( 'single_event.php' ) ) ) {
               $template_path = $theme_file;
           } else {
               $template_path = plugin_dir_path( __FILE__ ) . 'templates/single_event.php';
           }
       }
   }
   return $template_path;
}

add_filter( 'woocommerce_product_description_tab_title', 'isa_wc_description_tab_link_text', 999, 2 );
 
function isa_wc_description_tab_link_text( $text, $tab_key ) {
 
    return esc_html( 'Nutritional Information' );
 
}


add_filter('woocommerce_checkout_fields', 'custom_woocommerce_checkout_fields');

function custom_woocommerce_checkout_fields( $fields ) {

     $fields['order']['order_comments']['placeholder'] = 'Special instructions...';

     return $fields;
}
