<?php
/**
 * Template Name:Custom Advice Single Page
 *
 */
get_header(); ?>

<?php
global $post;
$mypost = array('post_type' => 'advices',);
$loop = new WP_Query($mypost);
//echo "<pre>"; print_r($post);
?>

<div class="custom__blogs_h">
<div class="t115-wrapper empty">
	
	<div class="t115-content">
<div class="i001-detail i001-image-right i001-image-med" id="i001-238321">
	<h1><?php the_title(); ?></h1>
		<div class="i001-detail-image">
			<div class="img">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
			</div>
		</div>
  <br>
	<div class="i001-detail-wrap">
		<div>
			<?php echo $post->post_content; ?>
     	</div>
     	
 </div>

</div>

</div>
	<div class="clearfix"></div>

		
</div>
</div>





 <?php wp_reset_query(); ?>

<?php get_footer(); ?>