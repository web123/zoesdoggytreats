<?php
/**
 * Template Name: Single Event
 *
 */
get_header(); ?>

<?php
global $post;
$mypost = array('post_type' => 'advices',);
$loop = new WP_Query($mypost);
//echo "<pre>"; print_r($post);
$start_time = get_field('start_time');  
					$end_time = get_field('end_time'); 
?>

<div class="custom__blogs_h" style="position: relative;">
<div class="t115-wrapper empty">
	
	<div class="t115-content">

<div class="i001-detail i001-image-right i001-image-med" id="i001-238321">
	<h1 style="padding-bottom: 10px"><?php the_title(); ?></h1>
<h5 style="color: rgb(0, 0, 0); font-family: arial; padding-bottom: 20px; text-transform: capitalize; font-size: 13px; font-weight: bold;"> <?php echo date('l F j Y', strtotime(get_field('date')));  ?>
									<br> <br> <?php if (strpos($end_time, 'pm') !== false) { echo $start_time." till ".$end_time;}
								  	else{ echo $start_time." - ".$end_time; }
								  ?> 
								 </h5>
		<div class="i001-detail-image">
			<div class="img">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
			</div>
		</div>
	<div class="i001-detail-wrap">
		<div>
			<?php echo $post->post_content; ?>
     	</div>
     	
 </div>

</div>

</div>
	<div class="clearfix"></div>

		
</div>
</div>
<br><br><br>
 <?php wp_reset_query(); ?>

<?php get_footer(); ?>