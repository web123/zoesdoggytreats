<?php
/**
 * Template Name:Custom Blog Single Page
 *
 */
get_header(); ?>

<?php
global $post;
$mypost = array('post_type' => 'advices',);
$loop = new WP_Query($mypost);
//echo "<pre>"; print_r($post);
?>

<div class="custom__blogs_h" style="position: relative;">
<div class="t115-wrapper empty">
	
	<div class="t115-content">

<div class="i001-extras0"><div class="i001-extras1"><div class="i001-extras2"><div class="i001-extras3">Comments: <span> <?php echo $my_var = get_comments_number($post->ID, 'full'); ?></span> 
</div></div></div></div>


<div class="i001-detail i001-image-right i001-image-med" id="i001-238321">
	<h1 style="padding-bottom: 10px"><?php the_title(); ?></h1>
<h5 style="padding-bottom: 20px"><?php echo date('j-n-Y h:i A', strtotime($post->post_date));  ?></h5>
		<div class="i001-detail-image">
			<div class="img">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
			</div>
		</div>
	<div class="i001-detail-wrap">
		<div>
			<?php echo $post->post_content; ?>
     	</div>
     	
 </div>

</div>

</div>
	<div class="clearfix"></div>

		
</div>

<div class="i001-comments-info" style="position: relative; margin-top: 50px; margin-bottom: 50px;">
	<div class="i001-extras0"><div class="i001-extras1"><div class="i001-extras2"><div class="i001-extras3">Comments: <span><?php echo $my_var = get_comments_number($post->ID, 'full'); ?></span></div></div></div></div>

		<h3>Comments</h3>
	

	<a href="javascript:void(0)" id="clickme" class="i001-css-button new_v01">Make a Comment</a>
	
	</div>

	<?php comment_form(); ?>



<?php 
$post_id = $post->ID;
$comments = get_comments('post_id= '.$post_id.'');
//echo "<pre>"; print_r($comments); 
foreach($comments as $comment) :
	
?>
	<div class="i001-comments-item ">
		<h4><?php echo($comment->comment_author); ?></h4>
		<h5> <?php echo date('j-n-Y h:i A', strtotime($comment->comment_date)); ?> - <?php echo($comment->comment_author); ?></h5>
		<?php echo($comment->comment_content); ?>
		</div>
<?php 
endforeach; ?>


</div>
 <?php wp_reset_query(); ?>

<script>
(function($) {
$( document ).ready(function() {
	$( "#clickme" ).click(function() {
  $( "#respond" ).toggle( "slow");
});
});}(jQuery));

</script>

<?php get_footer(); ?>