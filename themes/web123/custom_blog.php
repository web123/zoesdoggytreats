<?php
/**
 * Template Name:Custom Blog
 *
 */
get_header(); ?>

                        

<div class="custom__blogs_h">
<?php
            $args = array('post_type' => 'Advices', 'posts_per_page' => -1, 'order' => 'ASC' );
            $mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
            foreach ($posts as $post) { 
//print_r($post);
            	?>

	<div class="i001-list-item cms-mg-obj">
		<div class="i001-list-image">
			<a href="#">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
			</a>
		</div>
		<div class="i001-list-wrap">
			<h4>
				<a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a>
			</h4>
				
				<?php echo $trimmed = wp_trim_words( $post->post_content, $num_words = 35, $more = null )."..."; ?>
			
			<p> 
			<?php echo '<a type="button"  href="' . get_post_permalink($post->ID) . '" class="i001-css-button new_v01">Read More</a>'; ?>
			</p>
		

		</div>
	</div>
	<?php } ?>
</div>

<?php get_footer(); ?>